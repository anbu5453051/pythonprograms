#single inheritance

class parent():
    def fun(self):
        print(" Hello i am from parent class")
class child (parent):
    def fun1(self):
        print("Hello i am from child class")
obj=child()
obj.fun()
obj.fun1()


#multiple inheritance

class a:
    def a_detail(self,name,age):
        print("name :",name, "age :",age)
class b():
    def b(self,name,location):
        print("company name  :",name ,"Location :",location)
class c():
    def c_other(self,salary):
        print("salary :",salary )

class alldetails(a,b,c):
   # def all_detail(self):
        print("printing the all details :")

obj=alldetails()
obj.a_detail("anbu","25")
obj.b("chadura","musiri")
obj.c_other("20k")