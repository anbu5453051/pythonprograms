list=[1,2,3,4,"python"]
ite=iter(list)
print(next(ite))
print(next(ite))
print(next(ite))
print(next(ite))
print(next(ite))


print("___________________________________")

list=[1,2,3,4,"python"]
ite=iter(list)
#other method iterator print
print(ite.__next__())
print(ite.__next__())
print(ite.__next__())
print(ite.__next__())
print(ite.__next__())

print("______________________________")

def looping(value):
    ite=iter(value)
    while True:
        try:
            item=next(ite)
        except StopIteration:
            break
        else:
            print(item)
looping([1,2,3])