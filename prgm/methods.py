class details:
    def method(self):
        return"This is instance method",self
    @classmethod
    def classmethod(cls):
        return "This is class method",cls
    @staticmethod
    def staticmethod():
        return "This is static method"
    
obj=details()
print(obj.method())
print(obj.classmethod())
print(obj.staticmethod())
print(details.classmethod())